﻿<?php
session_start();


if(isset($_SESSION['id'])) {

    $connected = true;

    include_once('modele/blog/get_user_by_id.php');
    $user_info = get_user_by_id($_SESSION['id']);
    $nom = substr($user_info[0]['email'], 0, strpos($user_info[0]['email'], '@'));
    $avatar = $user_info[0]['avatar'];
	
    if(isset($_POST['edit_account']) and $_POST['edit_account'] == 'yes') {
        
        include_once('modele/blog/edit_account.php');
	    
		$id = $user_info[0]['id'];
		$nom = strip_tags($_POST['nom']);
		$email = htmlspecialchars($_POST['email']);
		$about = htmlspecialchars($_POST['about']);
		$avatar = $_FILES['avatar']['name'];
		$taille_image = $_FILES['avatar']['size'];
		$avatar_tmp = $_FILES['avatar']['tmp_name'];
		$error_upload = $_FILES['avatar']['error'];
		
		if(!empty($avatar) && $error_upload == 0){
		    $image_ext = strtolower(end(explode('.', $avatar))); //On recupere l'extension du fichier pour verifier qu'il s'agit bien d'une image
			
			if(in_array($image_ext, array('jpg', 'jpeg', 'png', 'gif')) && $taille_image <= 1000000){
			    $_SESSION['erreur_avatar'] = false;
			    move_uploaded_file($avatar_tmp, 'vue/blog/images/' . $avatar);
				$new_name = rand(200, rand(261, 2556)) . $_SESSION['id']. rand(rand(966, 5141), rand(20256, 59214)) . '.' . $image_ext;
				rename('vue/blog/images/'. $avatar, 'vue/blog/images/'. $new_name);
				$avatar = $new_name;
			    edit_account($id, $nom, $email, $about, $avatar);
				if($_POST['ancien_avatar'] != sha1('emagangamoalaincesardefaultavatarpardefaut').'.jpg'){
				    unlink('vue/blog/images/' . $_POST['ancien_avatar']);
				}
			} else{
			    if(in_array($image_ext, array('jpg', 'jpeg', 'png', 'gif'))){
				    $_SESSION['image_trop_grande'] = true;
				} else{
			        $_SESSION['erreur_avatar'] = true;
				}
			}
		} else {
		    if($error_upload != 0){
		        $_SESSION['image_trop_grande'] = true;
			} 
		    $avatar = $user_info[0]['avatar'];
		    edit_account($id, $nom, $email, $about, $avatar);
		}

        header('Location: ?page=profile');
    } else {
        include_once('vue/blog/profile.php');
    }
	
	if(isset($_POST['password']) and isset($_POST['password_confirmation'])){
	    if(!preg_match("#[a-zA-Z]{4,10}#", $_POST['password']) or strlen($_POST['password']) > 10) {
		    $_SESSION['wrong_password'] = true;
		} else {
		    if($_POST['password'] != $_POST['password_confirmation']){
			    $_SESSION['no_matching_password'] = true;
			} else {
			    include_once('modele/blog/reset_password.php');
				reset_password(sha1($_POST['password_confirmation']), $_SESSION['id']);
				$_SESSION['reset_successfull'] = true;
			}
		}
		
		header('Location: ?page=profile');
	}
	
} else {
    header('Location: ?page=login');
}