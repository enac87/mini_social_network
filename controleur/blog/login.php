﻿<?php

//gestion des erreurs de connection
//$pseudo_introuvable = false;
$invalid_pass = false;
$connected = false;
$wrong_user = false;

if(isset($_POST['email'])) {
    
    include_once('modele/blog/check_user.php');
    $check_user = check_user($_POST['email'], sha1($_POST['password']));
       
    if(!$check_user) {
        $wrong_user = true;
        include_once('vue/blog/login.php');
    } else {
        session_start();
        $_SESSION['id'] = $check_user[0]['id'];
		$_SESSION['nom'] = $check_user[0]['nom'];
		$_SESSION['email'] = $check_user[0]['email'];
		//$_SESSION['nom_log'] = substr($check_user[0]['email'], 0, strpos($check_user[0]['email'], '@'));
		$_SESSION['sexe'] = $check_user[0]['sexe'];
		//$_SESSION['about'] = $check_user[0]['about'];
        
        if(isset($_POST['auto_connect'])) {
            setcookie('email', $_SESSION['email'], time() + 365 * 24 * 3600, null, null, false, true);
	        setcookie('mot_de_passe', $_SESSION['mot_de_passe'], time() + 365 * 24 * 3600, null, null, false, true);
        }
        $connected = true;
        header('Location: ?page=membre');
    }   

} else if (isset($_COOKIE['email']) and isset($_COOKIE['mot_de_passe'])) {
    include_once('modele/blog/check_user_by_cookies.php');
    $check_user_by_cookies = check_user_by_cookies($_COOKIE['email']);
       
    if($check_user_by_cookies) {
        if($check_user_by_cookies[0]['mot_de_passe'] == $_COOKIE['mot_de_passe']) {
            $_SESSION['id'] = $check_user[0]['id'];
			$_SESSION['email'] = $check_user[0]['email'];
		    $_SESSION['date_inscription'] = $check_user[0]['date_inscription'];
		    $_SESSION['sexe'] = $check_user[0]['sexe'];
            $welcome = true;
            include_once('vue/blog/login.php');
        }
    } else { include_once('vue/blog/login.php'); }
} else {
     session_start();
    if(isset($_SESSION['id'])) {
	    header('Location: ?page=membre');
	} else {
        include_once('vue/blog/login.php');
    }		
}







    
