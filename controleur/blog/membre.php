﻿<?php
session_start();

if(isset($_SESSION['id'])) {
    $connected = true;
    include_once('modele/blog/get_user_by_id.php');

    $user_info = get_user_by_id($_SESSION['id']);
    $nom = substr($user_info[0]['email'], 0, strpos($user_info[0]['email'], '@'));
	$avatar = $user_info[0]['avatar'];

    include_once('vue/blog/membre.php');
	
} else {
    header('Location: ?page=login');
}