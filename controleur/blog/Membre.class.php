<?php
 
class Membre
{
    private $pseudo;
    private $pass;
    private $email;
    private $date_inscription;
    //private $signature;
    //private $actif;

    public function getPseudo()
    {
        return $this->pseudo;
    }

    public function setPseudo($nouveauPseudo)
    {
        if(!empty($nouveauPseudo) and strlen($nouveauPseudo) < 15)
        {
            $this->pseudo = $nouveauPseudo;
        }
    }

    public function envoyerEMail($titre, $message)
    {
        mail($this->email, $titre, $message);
    }

    public function bannir()
    {
        $this->actif = false;
        $this->envoyerEMail('Vous avez été banni', 'Ne revenez plus!');
    }
/*
    public function __destruct()
    {
        echo 'Cet objet va être détruit !';
    }
*/

}

 



