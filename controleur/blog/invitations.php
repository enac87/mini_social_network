﻿<?php
session_start();



if(isset($_SESSION['id'])) {

    $connected = true;

    include_once('modele/blog/get_user_by_id.php');

    $user_info = get_user_by_id($_SESSION['id']);
    $nom = substr($user_info[0]['email'], 0, strpos($user_info[0]['email'], '@'));
    $avatar = $user_info[0]['avatar'];
    
	include_once('modele/blog/get_invitations.php');
	
	$invitations_recues = get_invitations($_SESSION['id']);
	$nb_invitations_en_attente = invitations_en_attente($_SESSION['id']);
	
	if(isset($_GET['user']) && isset($_GET['action'])){
	include_once('modele/blog/get_user_by_id.php');
	    if(get_user_by_id($_GET['user'])){
		    if($_GET['action'] == 'accepted'){
			    invitation_accepted($_GET['user'], $_SESSION['id']);
				header('Location:?page=liste_membres&user=' . $_GET['user']);
			}else if($_GET['action'] == 'refused'){
			    invitation_refused($_GET['user'], $_SESSION['id']);
				header('Location:?page=invitations');
			}else{
			    header('Location:?page=invitations');
			}
		} else{
		    header('Location:?page=invitations');
		} 
	}
	
	
    include_once('vue/blog/invitations.php');
	
} else{
    header('Location: ?page=login');    
}