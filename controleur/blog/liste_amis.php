﻿<?php

session_start();


if(isset($_SESSION['id'])) {

    $connected = true;
	include_once('modele/blog/get_user_by_id.php');
    $user_info = get_user_by_id($_SESSION['id']);
    $nom = substr($user_info[0]['email'], 0, strpos($user_info[0]['email'], '@'));
	$avatar = $user_info[0]['avatar'];
	
	include_once('modele/blog/get_amis.php');
	$amis_exp = get_amis_exp($_SESSION['id']);
	$amis_dest = get_amis_dest($_SESSION['id']);
	include_once('vue/blog/liste_amis.php');
	
} else {
    header('Location: ?page=login');
}
