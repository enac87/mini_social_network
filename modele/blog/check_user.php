<?php

function check_user($email, $pass) //prend un mot de passe hache en parametre car les mots de passe sont haches avant d'etre sauvegardees
{                          //lors de l'inscription
    global $bdd;
    $email = (string) $email;
    $pass = (string) $pass;

    $req_lookup = $bdd->prepare('SELECT * FROM membres WHERE email = :email AND pass = :pass');
    $req_lookup->bindParam(':email', $email, PDO::PARAM_STR);
    $req_lookup->bindParam(':pass', $pass, PDO::PARAM_STR);
    $req_lookup->execute();   
    $resultat = $req_lookup->fetchAll();    
 
    return $resultat;
}
