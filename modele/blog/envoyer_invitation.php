﻿<?php

function envoyer_invitation($id_expediteur_, $id_destinataire_) {

    global $bdd;
	
	$id_expediteur_ = (string) $id_expediteur_;
	$id_destinataire_ = (string) $id_destinataire_;
 
    
    $send_invitations = $bdd->prepare('INSERT INTO amis(id_invitation, id_expediteur, id_destinataire, date_invitation, date_confirmation, invitation_acceptee)
	                                   VALUES("", :id_expediteur_, :id_destinataire_, NOW(), "", 0)');
	
    $send_invitations->bindParam(':id_expediteur_', $id_expediteur_, PDO::PARAM_STR);
	$send_invitations->bindParam(':id_destinataire_', $id_destinataire_, PDO::PARAM_STR);
	$send_invitations->execute();

}


//Cette fonction verifie si le destinataire a accepte la demande

function demande_acceptee($id_expediteur_, $id_destinataire_) {

    global $bdd;
	
	$id_expediteur_ = (string) $id_expediteur_;
	$id_destinataire_ = (string) $id_destinataire_;
	
	
	$check_invitation = $bdd->prepare('
	                           SELECT invitation_acceptee FROM amis 
							   WHERE (id_expediteur = :id_expediteur_ AND id_destinataire = :id_destinataire_)
							   OR (id_expediteur = :id_destinataire_ AND id_destinataire = :id_expediteur_)');
	$check_invitation->bindParam(':id_expediteur_', $id_expediteur_, PDO::PARAM_STR);
	$check_invitation->bindParam(':id_destinataire_', $id_destinataire_, PDO::PARAM_STR);
	$check_invitation->execute();
    
	$resultat = $check_invitation->fetchAll();
	
	foreach($resultat as $cle => $resu)
	{
	    if($resu[$cle] == 0){
	        return false;
        } else {
	        return true;
	    }
	}
	
}


//Cette fonction verifie si le membre connecte est l'espediteur de la demande
function verifier_expediteur($id_utilisateur, $membre_selectionne){
    
	global $bdd;
	
	$id_utilisateur = (string) $id_utilisateur;
	$membre_selectionne = (string) $membre_selectionne;
	
	$req_verif = $bdd->prepare('SELECT COUNT(id_invitation) AS nb_invit_envoyees FROM amis
	                           WHERE id_expediteur = :id_utilisateur 
							   AND id_destinataire = :membre_selectionne');
							   
	$req_verif->bindParam(':id_utilisateur', $id_utilisateur, PDO::PARAM_STR);
	$req_verif->bindParam(':membre_selectionne', $membre_selectionne, PDO::PARAM_STR);
	$req_verif->execute();
	
	$resultat = $req_verif->fetch();
	
	return $resultat['nb_invit_envoyees'];
 
}


//Cette fonction verifie si le membre connecte a deja reçu une invitation du membre sélectionné
function invit_deja_recue($id_utilisateur, $membre_selectionne){
    
	global $bdd;
	
	$id_utilisateur = (string) $id_utilisateur;
	$membre_selectionne = (string) $membre_selectionne;
	
	$req_verif_exp = $bdd->prepare('SELECT COUNT(id_invitation) AS nb_invit_recues FROM amis
	                           WHERE id_expediteur = :membre_selectionne 
							   AND id_destinataire = :id_utilisateur');
							   
	$req_verif_exp->bindParam(':membre_selectionne', $membre_selectionne, PDO::PARAM_STR);
	$req_verif_exp->bindParam(':id_utilisateur', $id_utilisateur, PDO::PARAM_STR);
	$req_verif_exp->execute();
	
	$resultat = $req_verif_exp->fetch();
	
	return $resultat['nb_invit_recues'];
 
}






//Permet d'annuler une demande
function annuler_demande($id_expediteur_, $id_destinataire_){

global $bdd;
	
	$id_expediteur_ = (string) $id_expediteur_;
	$id_destinataire_ = (string) $id_destinataire_;
 
    
    $cancel = $bdd->prepare('DELETE FROM amis WHERE id_expediteur = :id_expediteur_ AND id_destinataire = :id_destinataire_');
	
    $cancel->bindParam(':id_expediteur_', $id_expediteur_, PDO::PARAM_STR);
	$cancel->bindParam(':id_destinataire_', $id_destinataire_, PDO::PARAM_STR);
	$cancel->execute();

}
