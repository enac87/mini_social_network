﻿<?php
//Cette fonction récupère les invitations dans la base de données

function get_invitations($id_destinataire_){

    global $bdd;
	$id_destinataire_ = (string) $id_destinataire_;
	
	
	$get_invitations = $bdd->prepare('
	SELECT id_expediteur, date_invitation, invitation_acceptee, avatar, email
	FROM amis
	INNER JOIN membres ON membres.id = amis.id_expediteur
	WHERE id_destinataire = :id_destinataire_
	ORDER BY date_invitation DESC
	');
	
	$get_invitations->bindParam(':id_destinataire_', $id_destinataire_, PDO::PARAM_STR);
	$get_invitations->execute();
	
    $resultat = $get_invitations->fetchAll();
    
	return $resultat;

}

//Cette fonction valide une inviation
function invitation_accepted($id_expediteur_, $id_destinataire_){

    global $bdd;
	
	$id_expediteur_ = (string) $id_expediteur_;
	$id_destinataire_ = (string) $id_destinataire_;
 
    
    $accept_invitation = $bdd->prepare('
	UPDATE amis SET invitation_acceptee = 1, date_confirmation = NOW()
	WHERE id_expediteur = :id_expediteur_ AND id_destinataire = :id_destinataire_
	');

    $accept_invitation->bindParam(':id_expediteur_', $id_expediteur_, PDO::PARAM_STR);
	$accept_invitation->bindParam(':id_destinataire_', $id_destinataire_, PDO::PARAM_STR);
	$accept_invitation->execute();

}


//Cette fonction rejette(annule / supprime) une invitation 
function invitation_refused($id_expediteur_, $id_destinataire_){

    global $bdd;
	
	$id_expediteur_ = (string) $id_expediteur_;
	$id_destinataire_ = (string) $id_destinataire_;
	
	$refuse_invitation = $bdd->prepare('DELETE FROM amis WHERE id_expediteur = :id_expediteur_ AND id_destinataire = :id_destinataire_');
	
	$refuse_invitation->bindParam(':id_expediteur_', $id_expediteur_, PDO::PARAM_STR);
	$refuse_invitation->bindParam(':id_destinataire_', $id_destinataire_, PDO::PARAM_STR);
	$refuse_invitation->execute();
	
}

//Cette fonction vérifie si l'utilisateur a des invitations en attente (non encore acceptées)
function invitations_en_attente($id_destinataire_){

    global $bdd;
	$id_destinataire_ = (string) $id_destinataire_;
	
	
	$count_invitations_en_attente = $bdd->prepare('
	SELECT COUNT(*) AS nb_invit_en_attente FROM amis
	WHERE id_destinataire = :id_destinataire_ 
	AND invitation_acceptee = 0
	');
	
	$count_invitations_en_attente->bindParam(':id_destinataire_', $id_destinataire_, PDO::PARAM_STR);
	$count_invitations_en_attente->execute();
	
    $resultat = $count_invitations_en_attente->fetch();
	
	return $resultat['nb_invit_en_attente'];

}

