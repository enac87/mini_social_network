<?php

function check_user_by_cookies($pseudo) 
{                          
    global $bdd;
    $pseudo = (string) $pseudo;

    $req_lookup = $bdd->prepare('SELECT * FROM membres WHERE pseudo = :pseudo');
    $req_lookup->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
    $req_lookup->execute();   
    $resultat = $req_lookup->fetchAll();    
 
    return $resultat;
}
