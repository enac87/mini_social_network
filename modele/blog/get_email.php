<?php

function get_email($email)
{
    global $bdd;
    $email = (string) $email;

    $req_lookup = $bdd->prepare('SELECT * FROM membres WHERE email = :email');
    $req_lookup->bindParam(':email', $email, PDO::PARAM_STR);
    $req_lookup->execute();
    $resultat = $req_lookup->fetchAll();
    
    return $resultat;
}
