<?php

function get_login($login)
{
    global $bdd;
    $email = (string) $login;
    $resultat = false;

    $req_lookup = $bdd->prepare('SELECT * FROM membres WHERE email = :email');
    $req_lookup->bindParam(':email', $email, PDO::PARAM_STR);
    $req_lookup->execute();
    
    if ($req_lookup->fetchAll()) { $resultat = true; }
    return $resultat;
}
