<?php

function add_member($email, $pass, $sexe)
{
    global $bdd;
    $email = (string) $email;
	$id = sha1('emagangamoalaincesar'.$email);
    $pass = (string) $pass;
	$sexe = (string) $sexe;
	$avatar = sha1('emagangamoalaincesardefaultavatarpardefaut') . '.jpg';
    
    $req_insert = $bdd->prepare('INSERT INTO membres(id, nom, email, pass, date_inscription, sexe, about, avatar) VALUES(:id, NULL, :email, :pass, CURDATE(), :sexe, NULL, :avatar)');    
    $req_insert->bindParam(':id', $id, PDO::PARAM_STR);
	$req_insert->bindParam(':email', $email, PDO::PARAM_STR);
    $req_insert->bindParam(':pass', $pass, PDO::PARAM_STR);
	$req_insert->bindParam(':sexe', $sexe, PDO::PARAM_STR);
    $req_insert->bindParam(':avatar', $avatar, PDO::PARAM_STR);

    $req_insert->execute();
}
