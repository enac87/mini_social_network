﻿<?php

function get_user_by_id($id) //récupère l'utilisateur à partir de son id
{                          
    global $bdd;
    $id = (string) $id;

    $req_lookup = $bdd->prepare('SELECT * FROM membres WHERE id = :id');
    $req_lookup->bindParam(':id', $id, PDO::PARAM_STR);
    $req_lookup->execute();  
	
    $resultat = $req_lookup->fetchAll();    
 
    return $resultat;
	
}


function demande_existe($id_expediteur_, $id_destinataire_) {

    global $bdd;
	
	$id_expediteur_ = (string) $id_expediteur_;
	$id_destinataire_ = (string) $id_destinataire_;
	
	$count_invitations = $bdd->prepare('
	                           SELECT COUNT(id_invitation) AS nb_invitations FROM amis 
							   WHERE (id_expediteur = :id_expediteur_ AND id_destinataire = :id_destinataire_)
							   OR (id_expediteur = :id_destinataire_ AND id_destinataire = :id_expediteur_)');
	$count_invitations->bindParam(':id_expediteur_', $id_expediteur_, PDO::PARAM_STR);
	$count_invitations->bindParam(':id_destinataire_', $id_destinataire_, PDO::PARAM_STR);
	$count_invitations->execute();
	
	$req_nb_invitations = $count_invitations->fetch();
	
	return $req_nb_invitations['nb_invitations'];

}
 