﻿<?php

function nombre_membres() {
    global $bdd;
	
	$req_nb_membres = $bdd->query('SELECT COUNT(id) AS nb_membres FROM membres');
	$nb_membres = $req_nb_membres->fetch();
	return $nb_membres['nb_membres'];
}	