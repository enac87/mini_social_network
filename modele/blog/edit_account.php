﻿<?php

function edit_account($id, $nom, $email, $about, $avatar) {
    global $bdd;
	
	$nom = (string) $nom;
	$email = (string) $email;
	$about = (string) $about;
	$id = (string) $id;
	$avatar = (string) $avatar;
	
	$req_update = $bdd->prepare('UPDATE membres SET nom = :nom, email = :email, about = :about, avatar = :avatar WHERE id = :id');    
    $req_update->bindParam(':nom', $nom, PDO::PARAM_STR);
	$req_update->bindParam(':email', $email, PDO::PARAM_STR);
    $req_update->bindParam(':about', $about, PDO::PARAM_STR);
	$req_update->bindParam(':id', $id, PDO::PARAM_STR);
	$req_update->bindParam(':avatar', $avatar, PDO::PARAM_STR);
    

    $req_update->execute();


}