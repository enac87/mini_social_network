﻿<?php
//La fonction qui récupère le nom et l'avatar des membres, sauf de celui connecté 

function get_pseudo_avatar($id) {
    
	global $bdd;
	$id = (string) $id;
	
	$req_get = $bdd->prepare('SELECT id, email, avatar FROM membres WHERE id != :id');
	$req_get->bindParam(':id', $id, PDO::PARAM_STR);
	$req_get->execute();
	
	$resultat = $req_get->fetchAll();
    
	return $resultat;
}