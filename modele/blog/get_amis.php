﻿<?php
//Fonction qui récupère la liste d'utilisateurs ayant accepté mon invitation

function get_amis_exp($my_id) {

    global $bdd;
	
	$my_id = (string) $my_id;
	
	$amis = $bdd->prepare('SELECT id_destinataire AS id, invitation_acceptee, avatar, email
	                      FROM amis
	                      INNER JOIN membres ON membres.id = amis.id_destinataire
						  WHERE id_expediteur = :my_id AND invitation_acceptee = 1');
	$amis->bindParam(':my_id', $my_id, PDO::PARAM_STR);
	$amis->execute();
	
	$resultat = $amis->fetchAll();
	
	return $resultat;
		
}


//Fonction qui récupère la liste d'utilisateurs dont j'ai accepté l'invitation

function get_amis_dest($my_id) {

    global $bdd;
	
	$my_id = (string) $my_id;
	
	$amis = $bdd->prepare('SELECT id_expediteur AS id, invitation_acceptee, avatar, email
	                      FROM amis
	                      INNER JOIN membres ON membres.id = amis.id_expediteur
						  WHERE id_destinataire = :my_id AND invitation_acceptee = 1');
	$amis->bindParam(':my_id', $my_id, PDO::PARAM_STR);
	$amis->execute();
	
	$resultat = $amis->fetchAll();
	
	return $resultat;
	
}
