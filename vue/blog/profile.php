﻿<html lang="fr">
  <head>
    <title>Mon blog - <?php echo $nom; ?></title>
    <meta charset="utf-8">
    <link href="vue/blog/style.css" rel="stylesheet" type="text/css">
  </head> 
  <body>
    <div class="bloc_page">
	  <div class="header">
	    <?php include_once('vue/blog/header.php'); ?>
	  </div>
      <div id="profile">
	    <h1 id="edit_h1">Modifier le compte</h1>
        <div class="user_info">
		  <form method="post" action="?page=profile" enctype="multipart/form-data">
		    <h3>Renseignements</h3>
			<div>
			  <span class="spans">Nom</span>
			  <div class="div_info">
			    <input class="edit_inputs" placeholder="Saisissez votre Nom" name="nom" size="30" type="text" value="<?php echo isset($user_info[0]['nom']) ? $user_info[0]['nom'] : ''; ?>" placeholder="Nom" />
			  </div>
			</div>
			<div>
			  <span class="spans">Sexe</span>
			  <div class="div_info">
			    <input class="edit_inputs" name="user_sexe" size="30" type="text" value="<?php echo isset($user_info[0]['sexe']) ? $user_info[0]['sexe'] : 'N/A'; ?>" readonly />
			  </div>
			</div>
			<div>
			  <span class="spans">Email (obligatoire)</span>
			  <div class="div_info">
			    <input type="email" class="edit_inputs" id="user_email" name="email" placeholder="Email (obligatoire)" size="30" value="<?php echo isset($user_info[0]['email']) ? $user_info[0]['email'] : 'N/A'; ?>" />
			  </div>
			</div>
			<div class="div_info">
			  <span class="spans">A propos de moi</span>
			  <div>
			    <textarea cols="40" id="about" name="about" rows="4" placeholder="A propos de moi"><?php echo $user_info[0]['about'] ? $user_info[0]['about'] : ''; ?></textarea>
			  </div>
			</div>
			<div>
			  
			  <span class="spans">Changez votre photo de profil</span>
			  <div>
			    <br>
			    <img src="vue/blog/images/<?php echo $avatar; ?>" height="110" width="110" alt="Avatar" style="border: black 1px solid;"/>
				<br><br>
			    <input id="user_profile_image" name="avatar" type="file" />
			  </div>
			  <span style="margin-right: 80px; margin-top: -130px; color: red; text-align: center; float: right;"><?php if(isset($_SESSION['erreur_avatar']) and $_SESSION['erreur_avatar'] == true) { echo "Veuillez choisir une image valide !"; $_SESSION['erreur_avatar'] = false;} else if(isset($_SESSION['image_trop_grande']) and $_SESSION['image_trop_grande'] == true) { echo "Veuillez choisir une image<br>inérieure ou égale à 1 Mo !"; $_SESSION['image_trop_grande'] = false;} ?></span><br /><br />
			</div>
			<br><br>
			<input type="hidden" name="ancien_avatar" value="<?php echo $avatar ?>">
			<input class="edit_submit_button" type="submit" value="Mettre à jour le profil" />
			<input type="hidden" name="edit_account" value="yes">
		  </form>
		</div>
		<div id="change_password">
		  <div>
		    <form method="post" action="?page=profile">
			  <span class="spans"><h3>Modifier le mot de passe</h3><h6><em>(Entre 4 et 10 caractères, @ accepté.)</em></h6></span>
			  <p <?php if(isset($_SESSION['wrong_password']) and $_SESSION['wrong_password'] == true){ echo 'style="color: red; text-align: center; float: right; margin-right: 50px;"'; }else{echo 'style="color: green; text-align: center; float: right; margin-right: 50px;"';}?> ><?php if(isset($_SESSION['wrong_password']) and $_SESSION['wrong_password'] == true){ echo "Le mot de passe est invalide !"; $_SESSION['wrong_password'] = false;}else if(isset($_SESSION['reset_successfull']) and $_SESSION['reset_successfull'] == true){ echo "Votre mot de passe a été<br>réinitialisé avec success !"; $_SESSION['reset_successfull'] = false;} ?></p>
			  <div>
			    <input class="edit_inputs" id="user_password" name="password" placeholder="Nouveau mot de passe" size="30" type="password" style="margin-bottom: 15px;" required>
			  </div>
			  <p <?php if(isset($_SESSION['no_matching_password']) and $_SESSION['no_matching_password'] == true){ echo 'style="color: red; text-align: center; float: right;"'; }else{echo 'style="color: green; text-align: center; float: right;"';}?> ><?php if(isset($_SESSION['no_matching_password']) and $_SESSION['no_matching_password'] == true){ echo "Les mots de passe ne sont pas identiques !";  $_SESSION['no_matching_password'] = false;} ?></p>
			  <div>
			    <input class="edit_inputs" id="user_password_confirmation" name="password_confirmation" placeholder="Confirmer le mot de passe" size="30" type="password" required>
			  </div>
			  <br>
			  <input class="edit_submit_button" type="submit" value="Modifier le mot de passe">
			</form>
		  </div>
		</div>
      </div>
	  <div class="footer">
	  <?php include_once('vue/blog/footer.php'); ?>
	  </div>  
    </div>  	
  </body>
</html>