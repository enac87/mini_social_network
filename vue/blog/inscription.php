<!Doctype html>
<html lang="fr">
  <head>
    <title>Mon blog - Inscription</title>
    <meta charset="utf-8" media="screen">
    <link href="vue/blog/style.css" rel="stylesheet" type="text/css">
  </head>
  
  <body>
    <div class="bloc_page">
      <div class="header">
	  <?php include_once('vue/blog/header.php'); ?>
	  </div>
      <div class="bloc_connection">
        <p class="erreur_connection">
        <?php 
		if ($existing_user) {
            echo "Cet utilisateur existe deja !";
        } else if ($email_non_valide) {
            echo "Cet email n'est pas valide !";;
        } else if ($no_matching_pass) {
            echo "Les deux mots de passe ne sont pas identiques !";
        } else if ($wrong_sex) {
		    echo "Veuillez indiquer de quel sexe vous êtes !";
		}
        ?>    
        </p>
        
        <h2>Inscrivez vous ici !</h2>
        <form method="post" action="?page=inscription">
		  <div>
		    <span class="spans">E-mail</span>
			<div class="div_info">
			  <input class="edit_inputs" name="email" size="30" type="email" placeholder="e-mail" required />
			</div>
		  </div>
          <div>
			<span class="spans">Mot de passe</span>
			<div class="div_info">
			  <input class="edit_inputs" name="mot_de_passe" size="30" type="password" required />
			</div>
		  </div>
	      <div>
			<span class="spans">Réécrire votre mot de passe</span>
			<div class="div_info">
			  <input class="edit_inputs" name="verif_mot_de_passe" size="30" type="password" required />
			</div>
		  </div>
		  <div>
			<span class="spans">Sexe</span>
			<select name="sexe" id="sexe">
			  <option value="blank" selected></option>
			  <option value="Homme">Homme</option>
			  <option value="Femme">Femme</option>
			</select>
		  </div>
		  <br><br>
          <input class="edit_submit_button" type="submit" value="S'inscrire" />
        </form>
		<p><a href="?page=login">Vous avez déjà un compte ?</a></p> 
      </div>
      <div class="footer">
	  <?php include_once('vue/blog/footer.php'); ?>
	  </div> 
    </div> 
  </body>
</html>
