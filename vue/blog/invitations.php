﻿<!Doctype html>
<html lang="fr">
  <head>
    <title>Mon blog - <?php echo $nom; ?></title>
    <meta charset="utf-8">
    <link href="vue/blog/style.css" rel="stylesheet" type="text/css" media="screen">
  </head> 
  <body>
    <div class="bloc_page">
	  <div class="header">
	    <?php include_once('vue/blog/header.php'); ?>
	  </div>
      <div class="content">
	    <h3>Vos invitations</h3>
		<?php
		if($invitations_recues == true && $nb_invitations_en_attente > 0){
            foreach($invitations_recues as $invitation){
			    if($invitation['invitation_acceptee'] == 0){
			    ?>
				<div class="user_view_profile" style="border: 1px dotted black; padding: 10px;">
				  <img src="vue/blog/images/<?php echo $invitation['avatar']; ?>" height="100" width="100" alt="Avatar" style="border: 1px black solid;">
				  <br>
				  <p>
				  <strong><?php echo substr($invitation['email'], 0, strpos($invitation['email'], '@')); ?></strong> a voulu vous ajouter comme ami(e)<br><br>
				  <a href="?page=invitations&user=<?php echo $invitation['id_expediteur']; ?>&action=accepted">Accepter</a>  |  <a href="?page=invitations&user=<?php echo $invitation['id_expediteur']; ?>&action=refused">Refuser</a>
				  </p>
				</div>
				<?php
				}
			}
			
        }else{
        ?>
		<div class="user_view_profile">
		  <p>Vous n'avez pas d'invitations !!!</p>
		</div>
		<?php
        }		
		?>
      </div>
	  <div class="footer">
	  <?php include_once('vue/blog/footer.php'); ?>
	  </div>	  
    </div>
  </body>
</html>
