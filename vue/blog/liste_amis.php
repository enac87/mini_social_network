﻿<!Doctype html>
<html lang="fr">
  <head>
    <title>Mon blog - <?php echo $nom; ?></title>
    <meta charset="utf-8">
    <link href="vue/blog/style.css" rel="stylesheet" type="text/css">
  </head> 
  <body>
    <div class="bloc_page">
	  <div class="header">
	    <?php include_once('vue/blog/header.php'); ?>
	  </div>
      <div class="content">
	    <h2>Vos amis</h2>
        <?php 
		if(!empty($amis_exp) || !empty($amis_dest)){
		    foreach($amis_exp as $ami)
		    {
		?>
          <div>
            <p><a href="?page=liste_membres&user=<?php echo $ami['id']; ?>"style="text-decoration: none;"><?php echo substr($ami['email'], 0, strpos($ami['email'], '@')); ?></a></p>
			<a href="?page=liste_membres&user=<?php echo $ami['id']; ?>"><img src="vue/blog/images/<?php echo $ami['avatar']; ?>" height="100px" width="100px" alt="avatar" style="border: black 1px solid;"></a>
          </div>
		  <br>
        <?php		  
		    }
		    foreach($amis_dest as $ami)
		    {
		    ?>
            <div>
              <p><a href="?page=liste_membres&user=<?php echo $ami['id']; ?>"style="text-decoration: none;"><?php echo substr($ami['email'], 0, strpos($ami['email'], '@')); ?></a></p>
			  <a href="?page=liste_membres&user=<?php echo $ami['id']; ?>"><img src="vue/blog/images/<?php echo $ami['avatar']; ?>" height="100px" width="100px" alt="avatar" style="border: black 1px solid;"></a>
            </div>
		    <br>
          <?php
		    }
		} else {
		    echo "<div class='error_alone_empty'>Vous n'avez pas encore d'amis pour l'instant !</div>";
		}
        ?>		
      </div>
	  <div class="footer">
	  <?php include_once('vue/blog/footer.php'); ?>
	  </div>	  
    </div>
  </body>
</html>
