﻿<!Doctype html>
<html lang="fr">
  <head>
    <title>Mon blog - Connection</title>
    <meta charset="utf-8" media="screen">
    <link href="vue/blog/style.css" rel="stylesheet" type="text/css">
  </head>
  
  <body>
    <div class="bloc_page">
      <div class="header">
	  <?php include_once('vue/blog/header.php'); ?>
	  </div>
      <div class ="bloc_connection">
      <?php
      if(!$connected) {
          if($wrong_user) {
              echo "<p class='erreur_connection'>Mauvais identifiant ou mot de passe !</p>";
          } ?>
        
        <h2>Connectez - vous !</h2>
        <form method="post" action="?page=login">
		  <div>
			<span class="spans">E-mail</span>
			<div class="div_info">
			  <input class="edit_inputs" name="email" size="30" type="email" placeholder="e-mail" required />
			</div>
		  </div>
          <div>
			<span class="spans">Mot de passe</span>
			<div class="div_info">
			  <input class="edit_inputs" name="password" size="30" type="password" placeholder="********" required />
			</div>
		  </div>
		  <div>
			<span class="spans">Rester connecté</span>
			<input type="checkbox" name="auto_connect">
		  </div>
          <br><br>
          <input class="edit_submit_button" type="submit" value="Se connecter" />    
        </form>
	    <p><a href="?page=inscription">Vous n'avez pas de compte ?</a></p>
      </div>
	  <div class="footer">
	  <?php include_once('vue/blog/footer.php'); ?>
	  </div>
    </div>
       
  <?php
  } else { ?>
  <?php  header('Location: ?page=membre'); ?>
  <?php } ?>
  </body>
</html>
