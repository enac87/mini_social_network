﻿<!Doctype html>
<html lang="fr">
  <head>
    <title>Mon blog - <?php echo $nom; ?></title>
    <meta charset="utf-8">
    <link href="vue/blog/style.css" rel="stylesheet" type="text/css">
  </head> 
  <body>
    <div class="bloc_page">
	  <div class="header">
	    <?php include_once('vue/blog/header.php'); ?>
	  </div>
      <div class="content">
	  <?php
	  if(!isset($_GET['user']))
	  {
	  ?>
        <h2>La liste des membres</h2>
		<?php 
		if(!empty($pseudo_avatars)){
		    foreach($pseudo_avatars as $pseudo_avatar)
		    {
		?>
          <div>
            <p><a href="?page=liste_membres&user=<?php echo $pseudo_avatar['id']; ?>"style="text-decoration: none;"><?php echo substr($pseudo_avatar['email'], 0, strpos($pseudo_avatar['email'], '@')); ?></a></p>
			<a href="?page=liste_membres&user=<?php echo $pseudo_avatar['id']; ?>"><img src="vue/blog/images/<?php echo $pseudo_avatar['avatar']; ?>" height="100px" width="100px" alt="avatar" style="border: black 1px solid;"></a>
          </div>
		  <br>
        <?php		  
		    }
		} else {
		    echo "<div class='error_alone_empty'>Vous êtes le seul membre pour l'instant !</div>";
		}
	  }
      else
      {	
        foreach($view_user_profile as $user_info)
		{
	  ?>
	    <div class="user_view_profile">
		<?php
		    if(demande_existe($_SESSION['id'], $_GET['user']) == 0)
            {
			    if(!isset($_GET['action'])){
                    echo "
				         <p>Vous n'êtes pas ami(e) avec " . substr($user_info['email'], 0, strpos($user_info['email'], '@')) . "<br>
					     <br><a href='?page=liste_membres&user=" . $user_info['id'] . "&action=invite'>Envoyer une invitation</a>
					     <br><br><a href='?page=liste_membres'>Retour à la liste des membres</a>
					     </p>";
            	}
			} 
			else
			{
			    if(!demande_acceptee($_SESSION['id'], $_GET['user']) && verifier_expediteur($_SESSION['id'], $_GET['user']) == 1){
                    echo "<p style='color: green;'>Demande envoyée
					<br><br><a href='?page=liste_membres&user=" . $user_info['id'] . "&action=cancel_invite'>Annuler la demande</a>
					<br><br><a href='?page=liste_membres'>Retour à la liste des membres</a></p>";
                } else if(!demande_acceptee($_SESSION['id'], $_GET['user']) && verifier_expediteur($_SESSION['id'], $_GET['user']) == 0){
                    echo '<p style="color: #CC6600;">Demande en cours...
					      <br><br><q> Vérifiez vos invitations </q> !
						  <br><br><a href="?page=liste_membres">Retour à la liste des membres</a>
						  </p>';
                }					
            }				
						
		?>  
		</div>
		<?php 
			if(demande_existe($_SESSION['id'], $_GET['user']) != 0 && demande_acceptee($_SESSION['id'], $_GET['user'])) 
			{
		?>
	    <br><br><br>
		<a href='?page=liste_amis' style="text-decoration:none;">Retour à la liste d'amis</a><br>
		<div id = "user_profile_view">
		  <div class="user_view_profile">
		    <h3><strong>Avatar</strong></h3>
	        <p><img src="vue/blog/images/<?php echo $user_info['avatar'];?>" height="200" width="200" alt="Avatar" style="border: 1px black solid;"/></p>
		  </div>
		  <div class="user_view_profile">
		    <h3><strong>Email</strong></h3>
			<p><em><?php echo $user_info['email']; ?></em></p>
		  </div>
		  <div class="user_view_profile">
		    <h3><strong>Sexe</strong></h3>
			<p><em><?php echo $user_info['sexe']; ?></em></p>
		  </div>
		  <div class="user_view_profile">
		    <h3><strong>A propos de moi</strong></h3>
			<p><em><?php echo nl2br($user_info['about']); ?></em></p>
		  </div>
		</div>
        <br>		
		<a href='#' style="color:red; text-decoration:none">Le supprimer de votre liste d'amis</a>
	  <?php
	        }
        }
	  }
      ?>	  
      </div>
	  <div class="footer">
	  <?php include_once('vue/blog/footer.php'); ?>
	  </div>	  
    </div>
  </body>
</html>
