﻿ <html lang="fr">
  <head>
    <title>Mon blog - <?php echo $_SESSION['pseudo']; ?></title>
    <meta charset="utf-8" media="screen">
    <link href="vue/blog/style.css" rel="stylesheet" type="text/css">
  </head> 
  <body>
    <div id="bloc_page">
    <?php include_once('vue/blog/header.php'); ?>
      <div class="body">
        <div class="user_info">
		  <form method="post" action="">
		    <table class="table_info">
		      <tr>
			    <td class="first_td"><strong>Prénom</strong></td>
			    <td style="white-space: nowrap;"><input style="height:100%" value="<?php echo isset($_SESSION['prenom']) ? $_SESSION['prenom'] : 'N/A'; ?>" required /></td>
			  </tr>
			  <tr>
			    <td class="first_td"><strong>Nom</strong></td>
			    <td><input value="<?php echo isset($_SESSION['nom']) ? $_SESSION['nom'] : 'N/A'; ?>" required /></td>
			  </tr>
			  <tr>
			    <td class="first_td"><strong>Age</strong></td>
			    <td><input value="<?php echo isset($_SESSION['age']) ? $_SESSION['age'] : 'N/A'; ?>" required /></td>
			  </tr>
			  <tr>
			    <td class="first_td"><strong>Sexe</strong></td>
			    <td><input value="<?php echo isset($_SESSION['sexe']) ? $_SESSION['sexe'] : 'N/A'; ?>" required /></td>
			  </tr>
		      <tr>
			    <td class="first_td"><strong>Email</strong></td>
			    <td><input value="<?php echo $_SESSION['email']; ?>" /></td>
			  </tr>
			  <tr>
			    <td class="first_td"><strong>Numéro mobile</strong></td>
			    <td><input value="<?php echo isset($_SESSION['phone']) ? $_SESSION['phone'] : 'N/A'; ?>" required /></td>
			  </tr>
		    </table>
			<p><input type="submit" value="Enregistrer"></p>
			<p><a class="edit_cancel_button" href="?page=profile">Annuler</a></p>
		  </form>	
		</div>
      </div>
    <!-- <?php include_once('vue/blog/footer.php'); ?> -->
    </div>  
  </body>
</html>